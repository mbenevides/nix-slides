{ pkgs ? import ./nix/pinned-nixpkgs.nix {} }:

let
  gitignoreSource = import ./nix/gitignore.nix {};
  texliveEnv = (import ./nix/texlive.nix {}).env;
in
pkgs.stdenv.mkDerivation {
  name = "slides";
  src = gitignoreSource ./.;
  buildInputs = [ texliveEnv ];
  buildPhase = ''
    mkdir $out
    latexmk -verbose -pdf \
      -pdflatex="pdflatex -interaction=nonstopmode -halt-on-error -shell-escape"\
      slide.tex
  '';

  installPhase = ''
    cp slide.pdf $out/slide.pdf
  '';
}

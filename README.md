# An intro to Nix (pt-BR)

## Building

```
nix-build
```

## Development environment

```
nix-shell
```

## Slides

The latest build of the slides can be downloaded [here](https://gitlab.com/mbenevides/nix-slides/-/jobs/artifacts/master/download?job=build).

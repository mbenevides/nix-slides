{ pkgs ? import ./pinned-nixpkgs.nix {} }:

{
  env = pkgs.texlive.combine {
    inherit (pkgs.texlive)
      beamer
      beamertheme-metropolis
      ccicons
      collection-langportuguese
      collection-latexextra
      collection-mathscience
      hyphen-portuguese
      latexmk
      listings
      pgf
      xcolor
      ;
  };
}
